name := """test-relations"""
organization := "com.sandbox"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  guice,
  javaJdbc,
  evolutions,
  "org.projectlombok" % "lombok" % "1.16.16" % "provided",
  "org.postgresql" % "postgresql" % "42.1.1"
)
