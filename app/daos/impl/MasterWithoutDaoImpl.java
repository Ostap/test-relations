package daos.impl;

import daos.MasterWithoutDao;
import models.MasterWithout;

import java.util.Optional;


public class MasterWithoutDaoImpl implements MasterWithoutDao {

    @Override
    public Optional<MasterWithout> getById(Long id) {
        return MasterWithout.find.query().where()
                .eq("id", id)
                .findOneOrEmpty();
    }
}
