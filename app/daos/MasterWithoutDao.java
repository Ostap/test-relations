package daos;

import com.google.inject.ImplementedBy;
import daos.impl.MasterWithoutDaoImpl;
import models.MasterWithout;

import java.util.Optional;

@ImplementedBy(MasterWithoutDaoImpl.class)
public interface MasterWithoutDao {

    Optional<MasterWithout> getById(Long id);
}
