package models;

import io.ebean.Finder;
import io.ebean.Model;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Builder
public class SlaveWithout1 extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    @Version
    private Timestamp lastUpdated;

    public static final Finder<Long, SlaveWithout1> find = new Finder<>(SlaveWithout1.class);
}
