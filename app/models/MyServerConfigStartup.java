package models;

import io.ebean.config.ServerConfig;
import io.ebean.config.dbplatform.DbIdentity;
import io.ebean.config.dbplatform.IdType;
import io.ebean.config.dbplatform.postgres.PostgresPlatform;
import io.ebean.event.ServerConfigStartup;

public class MyServerConfigStartup implements ServerConfigStartup {
    public void onStart(ServerConfig serverConfig) {
        serverConfig.setDatabaseSequenceBatchSize(1);
        PostgresPlatform postgresPlatform = new PostgresPlatform();
        DbIdentity dbIdentity = postgresPlatform.getDbIdentity();
        dbIdentity.setSupportsGetGeneratedKeys(false);
        dbIdentity.setSupportsSequence(true);
        //dbIdentity.setIdType(IdType.SEQUENCE);
        serverConfig.setDatabasePlatform(postgresPlatform);
    }
}