package models;

import io.ebean.Finder;
import io.ebean.Model;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Builder
public class SlaveWithout2 extends Model {

    @Id
    private String naturalId;

    private String name;

    @Version
    private Timestamp lastUpdated;

    public static final Finder<String, SlaveWithout2> find = new Finder<>(SlaveWithout2.class);
}
