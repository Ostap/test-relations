import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import play.api.db.Database;

import java.sql.Connection;

public class Module extends AbstractModule{
    @Override
    protected void configure() {

    }

    @Singleton
    @Provides
    public Connection getJdbcConnection(Database database) {
        return database.getConnection();
    }
}
