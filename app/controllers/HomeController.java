package controllers;

import daos.MasterWithoutDao;
import models.*;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.Optional;

public class HomeController extends Controller {

    private final MasterWithoutDao masterWithoutDao;

    @Inject
    public HomeController(MasterWithoutDao masterWithoutDao) {
        this.masterWithoutDao = masterWithoutDao;
    }

    public Result index() {
       createWithout();
        updateWithout();
        return ok(views.html.index.render());
    }

    private void createWithout() {
        MasterWithout masterWithout = MasterWithout.builder().name("Würzburg").swo1(SlaveWithout1.builder().name("swo1").build()).build();
        masterWithout.insert();
    }

    private void updateWithout() {
        Optional<MasterWithout> byId = masterWithoutDao.getById(1L);
        MasterWithout masterWithout = byId.get();
        SlaveWithout2 swo2 = SlaveWithout2.builder().naturalId("ni").name("swo2").build();
        swo2.insert();
        masterWithout.setSwo2(swo2);
        masterWithout.update();
    }

}
