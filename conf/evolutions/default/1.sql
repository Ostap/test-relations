# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table master_without (
  id                            bigint not null,
  name                          varchar(255),
  swo1_id                       bigint,
  swo2_natural_id               varchar(255),
  last_updated                  timestamptz not null,
  constraint uq_master_without_swo1_id unique (swo1_id),
  constraint uq_master_without_swo2_natural_id unique (swo2_natural_id),
  constraint pk_master_without primary key (id)
);
create sequence master_without_seq;

create table slave_without1 (
  id                            bigint not null,
  name                          varchar(255),
  last_updated                  timestamptz not null,
  constraint pk_slave_without1 primary key (id)
);
create sequence slave_without1_seq;

create table slave_without2 (
  natural_id                    varchar(255) not null,
  name                          varchar(255),
  last_updated                  timestamptz not null,
  constraint pk_slave_without2 primary key (natural_id)
);

alter table master_without add constraint fk_master_without_swo1_id foreign key (swo1_id) references slave_without1 (id) on delete restrict on update restrict;

alter table master_without add constraint fk_master_without_swo2_natural_id foreign key (swo2_natural_id) references slave_without2 (natural_id) on delete restrict on update restrict;


# --- !Downs

alter table if exists master_without drop constraint if exists fk_master_without_swo1_id;

alter table if exists master_without drop constraint if exists fk_master_without_swo2_natural_id;

drop table if exists master_without cascade;
drop sequence if exists master_without_seq;

drop table if exists slave_without1 cascade;
drop sequence if exists slave_without1_seq;

drop table if exists slave_without2 cascade;

